$connectionstring = "server=\\.\pipe\MICROSOFT##WID\tsql\query;database=SUSDB;trusted_connection=true;"

$SQLConnection= New-Object System.Data.SQLClient.SQLConnection($ConnectionString)

$SQLConnection.Open()

$SQLCommand = $SQLConnection.CreateCommand()

$SQLCommand.CommandText = 'spGetAllUpdateUrls'

$SQLCommand.CommandType = [System.Data.CommandType]::StoredProcedure

$SqlDataReader = $SQLCommand.ExecuteReader()

$SQLDataResult = New-Object System.Data.DataTable

$SQLDataResult.Load($SqlDataReader)

$SQLConnection.Close()

#$SQLDataResult |select UpdateID, FileDigest |Export-Csv E:\export.csv -NoTypeInformation

$enc = [System.Text.Encoding]::UTF8

$SQLDataResult |ForEach-Object {

    [PSCustomObject]@{

        UpdateID = $_.UpdateID

        #FileDigest = [string]$_.FileDigest -replace " ",""

        #FileDigest = $temp -replace " ",""

        FileDigest = ($_.FileDigest|ForEach-Object ToString X2) -join ""

    }

}|Export-Csv E:\export.csv –NoTypeInformation