SELECT        u.UpdateID, lp.Title AS DefaultTitle, lp.Description AS DefaultDescription,
                             (SELECT        TOP (1) KBArticleID
                               FROM            dbo.tbKBArticleForRevision
                               WHERE        (RevisionID = r.RevisionID)
                               ORDER BY KBArticleID DESC) AS KnowledgebaseArticle,
                             (SELECT        TOP (1) SecurityBulletinID
                               FROM            dbo.tbSecurityBulletinForRevision
                               WHERE        (RevisionID = r.RevisionID)
                               ORDER BY SecurityBulletinID DESC) AS SecurityBulletin
FROM            dbo.tbUpdate AS u INNER JOIN
                         dbo.tbRevision AS r ON u.LocalUpdateID = r.LocalUpdateID INNER JOIN
                         dbo.tbProperty AS p ON r.RevisionID = p.RevisionID INNER JOIN
                         dbo.tbRevisionInCategory AS rc ON rc.RevisionID = r.RevisionID AND rc.Expanded = 0 INNER JOIN
                         dbo.tbUpdate AS uc ON uc.LocalUpdateID = rc.CategoryID INNER JOIN
                         dbo.tbCategory AS c ON uc.LocalUpdateID = c.CategoryID AND c.CategoryType = N'UpdateClassification' INNER JOIN
                         dbo.tbLocalizedPropertyForRevision AS lpr ON lpr.RevisionID = r.RevisionID AND lpr.LanguageID = '1036' /*p.DefaultPropertiesLanguageID*/ INNER JOIN
                         dbo.tbLocalizedProperty AS lp ON lp.LocalizedPropertyID = lpr.LocalizedPropertyID
WHERE        (r.IsLatestRevision = 1) AND (p.ExplicitlyDeployable = 1)

	/*SELECT     u.UpdateID, r.RevisionNumber, F.FileDigest, F.MUURL, F.USSURL
FROM       --  dbo.tbUpdate u INNER JOIN
           --  dbo.tbRevision r ON u.LocalUpdateID = r.LocalUpdateID INNER JOIN
             dbo.tbFileForRevision FFR ON FFR.RevisionID= r.RevisionID INNER JOIN
             dbo.tbFile F ON FFR.FileDigest = F.FileDigest 
WHERE 
		u.IsLocallyPublished = 0*/